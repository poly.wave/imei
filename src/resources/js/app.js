
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('../../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});

const makeId = (length) => {
    let result           = '';
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;

    for ( let i = 0; i < length; i ++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
};

const settings = {
  captchaUrl: '/captcha/default'
};

$(document).ready(() => {
    const loader = $('#loader');

    $('body').on('submit', '#check', function (event) {
        let self = $(this);

        let html = '<div class="message">' +
                '<img src="/images/warning.jpg" class="warning-img">' +
                '<div class="clearfix" style="padding: 20px;">' +
                    '<button id="cancel" class="btn btn-md btn-danger float-left">Отменить</button>' +
                    '<button id="accept" class="btn btn-md btn-success float-right">Продолжить</button>' +
                '</div>' +
            '</div>';

        $.fancybox.open(html);

        return false;
    });

    $('body').on('click', '#cancel', function (event) {
        $.fancybox.close();

        return false;
    });

    $('body').on('click', '#accept', function (event) {
        $.fancybox.close();

        let form = $('#check');

        loader.fadeIn();

        $.ajax({
            url: form.attr('action'),
            type: form.attr('method'),
            data: form.serialize(),
            dataType: 'json',
            success: function(response) {
                loader.hide();

                let src = settings.captchaUrl + '?' + makeId(8);
                form.find('img').attr('src', src);

                if(response.status == true) {
                    $.fancybox.open('<div class="message">' + response.data + '</div>');
                } else {
                    $.fancybox.open('<div class="message" style="color: red; padding: 40px;">' + response.message + '</div>');
                }
            }
        });

        return false;
    });
});
