@extends('layouts.app')

@section('content')
    <header>
        <div class="row mobile">
            <div class="column col-sm-12">
                <img src="/images/33.png">
            </div>
        </div>

        <div class="row">
            <div class="column col-sm-5 first">
                <img src="/images/11.png">
            </div>

            <div class="column col-sm-2 logo">
                <img src="/images/logo.png" >
            </div>

            <div class="column col-sm-5 last">
                <img src="/images/22.png">
            </div>
        </div>
    </header>

    <div class="row">
        <div class="col-sm-12" style="font-size: 20px; text-align: center; margin-bottom: 20px;">
            ЕДИНСТВЕННАЯ<br>
            БЕСПЛАТНАЯ - ТОЧНАЯ - ПОЛНАЯ<br>
            ПОНЯТНАЯ - ПРОВЕРКА IMEI
        </div>
    </div>

    <form method="post" action="/check" id="check">
        @csrf

        <div class="form-group">
            <input type="text" name="imei" placeholder="IMEI/SERIAL" class="form-control form-control-lg" required>
        </div>

        <div class="form-group">
            <img src="{{ captcha_src() }}" style="width: 50%;">
        </div>

        <div class="form-group">
            <input type="text" name="captcha" placeholder="Код с картинки" class="form-control form-control-lg" required>
        </div>

        <div class="form-group">
            <button type="submit" name="check" class="btn btn-lg btn-primary">Проверить</button>
        </div>
    </form>

    <div class="row">
        <div class="col-sm-12" style="font-size: 16px; color: green; text-align: center; margin-top: 20px;">
            Пишите в <a href="https://api.whatsapp.com/send?phone=79186757579&text=%D0%97%D0%B4%D1%80%D0%B0%D0%B2%D1%81%D1%82%D0%B2%D1%83%D0%B9%D1%82%D0%B5.%20%D0%AF%20%D0%BF%D0%BE%20%D0%B2%D0%BE%D0%BF%D1%80%D0%BE%D1%81%D1%83%20-%20" target="_blank" class="btn btn-sm btn-success">WhatsApp</a>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6" style="margin-top: 20px;">
            <iframe src="https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=%D0%9F%D0%BE%D0%B4%D0%B4%D0%B5%D1%80%D0%B6%D0%B8%D1%82%D0%B5%20%D1%81%D0%B0%D0%B9%D1%82%20%F0%9F%92%AA%20%D0%A1%D0%BF%D0%B0%D1%81%D0%B8%D0%B1%D0%BE!&targets-hint=&default-sum=99&button-text=12&payment-type-choice=on&mobile-payment-type-choice=on&hint=&successURL=http%3A%2F%2Fproverka-imei.ru&quickpay=shop&account=410016952859192" width="100%" height="222" frameborder="0" allowtransparency="true" scrolling="no"></iframe>
        </div>
        <div class="col-sm-3"></div>
    </div>
@endsection
