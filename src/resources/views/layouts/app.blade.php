<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="/favicon.jpg" type="image/x-icon">

    <title>ЕДИНСТВЕННАЯ - БЕСПЛАТНАЯ - ТОЧНАЯ - ПОЛНАЯ - ПОНЯТНАЯ - ПРОВЕРКА IMEI/ Остальные показывают погоду</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
    <div id="app">
        @include('layouts.header')
        <div class="app-body">
            <div class="container-fluid">
                @if(isset($success))
                    <div role="alert" class="alert alert-success">
                        {{ $success }}
                    </div>
                @endif

                @if(isset($danger))
                    <div role="alert" class="alert alert-danger">
                        {{ $danger }}
                    </div>
                @endif

                <div>
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    <footer>PROVERKA IMEI &copy; {{ date('Y') }}</footer>

    <div id="loader">
        <div class="cssload-thecube">
            <div class="cssload-cube cssload-c1"></div>
            <div class="cssload-cube cssload-c2"></div>
            <div class="cssload-cube cssload-c4"></div>
            <div class="cssload-cube cssload-c3"></div>
        </div>
    </div>

    <script id='chat-24-widget-code' type="text/javascript">
        !function (e) {
            var t = {};
            function n(c) { if (t[c]) return t[c].exports; var o = t[c] = {i: c, l: !1, exports: {}}; return e[c].call(o.exports, o, o.exports, n), o.l = !0, o.exports }
            n.m = e, n.c = t, n.d = function (e, t, c) { n.o(e, t) || Object.defineProperty(e, t, {configurable: !1, enumerable: !0, get: c}) }, n.n = function (e) {
                var t = e && e.__esModule ? function () { return e.default } : function () { return e  };
                return n.d(t, "a", t), t
            }, n.o = function (e, t) { return Object.prototype.hasOwnProperty.call(e, t) }, n.p = "/packs/", n(n.s = 0)
        }([function (e, t) {
            window.chat24WidgetCanRun = 1, window.chat24WidgetCanRun && function () {
                window.chat24ID = "0422b33ae71dce2bcafe27b1c220bd64", window.chat24io_lang = "ru";
                var e = "https://livechat.chat2desk.com", t = document.createElement("script");
                t.type = "text/javascript", t.async = !0, fetch(e + "/packs/manifest.json").then(function (e) {
                    return e.json()
                }).then(function (n) {
                    t.src = e + n["widget.js"];
                    var c = document.getElementsByTagName("script")[0];
                    c ? c.parentNode.insertBefore(t, c) : document.documentElement.firstChild.appendChild(t);
                    var o = document.createElement("link");
                    o.href = e + n["widget.css"], o.rel = "stylesheet", o.id = "chat-24-io-stylesheet", o.type = "text/css", document.getElementById("chat-24-io-stylesheet") || document.getElementsByTagName("head")[0].appendChild(o)
                })
            }()
        }]);
    </script>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter49421182 = new Ya.Metrika2({
                        id:49421182,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/tag.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks2");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/49421182" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
</body>
</html>
