<?php

return [
    'default' => [
        'length'    => 4,
        'width'     => 240,
        'height'    => 72,
        'quality'   => 90,
        'math'      => false, //Enable Math Captcha
    ],
];
