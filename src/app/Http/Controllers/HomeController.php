<?php

namespace App\Http\Controllers;

use App\Http\Packages\Sickw;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
    }

    public function check()
    {
        $validator = Validator::make($this->request->all(), [
            'captcha' => 'required|captcha'
        ]);

        if ($validator->fails()) {
            return $this->json([
                'status' => false,
                'message' => 'Код с картинки введен неверно',
                'errors' => $validator->messages(),
            ]);
        } else {
            $validator = Validator::make($this->request->all(), [
                'imei' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->json([
                    'status' => false,
                    'message' => 'Вы не ввели IMEI/SERIAL',
                    'errors' => $validator->messages(),
                ]);
            } else {
                $sickw = new Sickw();

                $html = $sickw->request($this->request->input('imei'));
                $parse = $sickw->parse($html);
                $data = $sickw->out_html($html, $parse);

                return $this->json([
                    'status' => true,
                    'message' => 'Успешная валидация',
                    'data' => $data
                ]);
            }
        }
    }

    public function index()
    {
        return $this->render('home');
    }
}
