<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class CheckController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;
    }
}
