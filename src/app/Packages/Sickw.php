<?php

namespace App\Http\Packages;

use phpDocumentor\Reflection\Types\Object_;
use Sunra\PhpSimple\HtmlDomParser;

class Sickw
{
    private $apiKey = '95T-13B-R6K-TK3-HDH-ZKT-J9T-7M1';
    private $apiUrl = 'https://sickw.com/api.php';
    private $service = 30;

    public function request(string $imei): string
    {
        $curl = new Curl();
        $params = [
            'show' => 'no',
            'key' => $this->apiKey,
            'imei' => $imei,
            'service' => $this->service
        ];
        $url = $this->apiUrl . '?' . http_build_query($params);

        $response = $curl->request($url);

        return $response;
    }

    public function out_html($response, $parse): string
    {
        $result = '';

        $error = '
            <b>
              <span style="color:red;">Вы переписали ВСЕ 15 цифр IMEI?<br>Перепишите не MEID, а IMEI !<br><br>ПРОВЕРЬТЕ, правильно ли вы ПЕРЕПИСАЛИ IMEI/SERIAL?</span><br><br>
              <span style="color:green;">Если да</span> - нужно заказывать информацию.<br>
              <span style="color:green;">Напишите нам в WhatsApp 📲</span>
            </b>
            <br>
        ';

        $noimei = '
            <b>
              По данному IMEI нет информации в базе, нужно заказывать GSX-отчет.<br>
              <span style="color:green;">Напишите нам в WhatsApp.</span>
            </b>
            <br>
        ';

        $nodevice = '
            <b>
              Нужно разблокировать?<br>
              <span style="color:green;">Напишите нам в WhatsApp.</span>
            </b>
            <br>
        ';

        $lost = '
            <b>
              <span style="color:red;"> Статус iCloud: LOST.<br>
              <span style="color:black;">Данное устройство в режиме<br>КРАЖИ / ПРОПАЖИ.</span><br>
              <span style="color:red;"> 🚫 Отказ. 🚫<br></span>
            </b><br>
        ';

        $clean = '
            <b>
              <span style="font-weight:900">
                <span style="color:green">Данный IMEI чистый, не в пропаже.</span><br>
                 <br>
                Для разблокировки/удаления iCloud и уточнения стоимости -<span style="color:green;"> ПРИШЛИТЕ нам в WhatsApp<br>Фото экрана с IMEI + Перепишите IMEI.<br> 
              </span> 
            </b>
            </span><br>
        ';

        $off = '
            <b>
              <span style="color:green; font-size:14px;">Функция «Найти iPhone» - Выключена.<br>
              Данное устройство Не Привязано к iCloud.<br>
              Разблокировка iCloud не нужна.</span><br>
              <span style="color:black; font-size:12px;">Прошейте его в DFU-режиме до заводских настроек 
              и активируйте как новый.<br></span>
            </b>
            <br>
        ';

        $locked = '
            <b>
              <span style="color:red;">Данное устройство залочено под Оператора.</span><br>
              <span style="color:green; font-size:14px;">Для уточнения возможности отвязки от ператора и стоимости - напишите нам в WhatsApp.</span>
            </b>
            <br>
        ';



        if (isset($parse->error)) {
            $result = $error;
        } else {
            if(
                strpos($response, 'Wrong') ||
                mb_substr_count($response, 'IMEI or SN is incorrect!')
            ) {
                $result = $error;
            } else {
                $exec = true;

                if(empty($parse->icloud_lock)){
                    if(empty($parse->image_url->data)){
                        $result = $noimei;

                        $exec = false;
                    }else if(empty($parse->model->data_low)){
                        $result = $nodevice;

                        $exec = false;
                    }
                }else{
                    if(mb_substr_count($parse->icloud_lock->data_low, 'off')){
                        $result = $off;

                        $exec = false;
                    }
                }

                if($exec == true){
                    if(mb_substr_count($parse->sim_lock->data_low, 'unlocked') == 0 && mb_substr_count($parse->sim_lock->data_low, 'locked')){
                        $result = $locked;
                    }else if(mb_substr_count($parse->icloud_status->data_low, 'lost')){
                        $result = $lost;
                    }else{
                        if(
                            mb_substr_count($parse->part_number_country->data_low, 'usa') ||
                            mb_substr_count($parse->part_number_country->data_low, 'mexico') ||
                            mb_substr_count($parse->part_number_country->data_low, 'canada')
                        ){
                            if(
                                mb_substr_count($parse->model->data_low, 'iphone 3g') ||
                                mb_substr_count($parse->model->data_low, 'iphone 3gs') ||
                                mb_substr_count($parse->model->data_low, 'iphone 4') ||
                                mb_substr_count($parse->model->data_low, 'iphone 4s') ||
                                mb_substr_count($parse->model->data_low, 'iphone 5') ||
                                mb_substr_count($parse->model->data_low, 'iphone 5c') ||
                                mb_substr_count($parse->model->data_low, 'iphone 5s') ||
                                mb_substr_count($parse->model->data_low, 'ipad') ||
                                mb_substr_count($parse->model->data_low, 'ipad 2') ||
                                mb_substr_count($parse->model->data_low, 'vin') ||
                                mb_substr_count($parse->model->data_low, 'ipad (3rd gen)') ||
                                mb_substr_count($parse->model->data_low, 'ipad 3')
                            ){
                                $result = '
                                    <b>
                                      <span style="color:black;">Данное устройство произведено для Америки<br>
                                      <span style="color:red;">Бюджетные устройства делать не выгодно.<br>
                                      <span style="color:red;">🚫 Отказ 🚫</span><br>
                                      </b><br>
                                  ';
                            }
                        }else if(
                            mb_substr_count($parse->part_number_country->data_low, 'china') ||
                            mb_substr_count($parse->part_number_country->data_low, 'japan') ||
                            mb_substr_count($parse->part_number_country->data_low, 'qatar') ||
                            mb_substr_count($parse->part_number_country->data_low, 'kuwait') ||
                            mb_substr_count($parse->part_number_country->data_low, 'malaysia') ||
                            mb_substr_count($parse->part_number_country->data_low, 'singapore') ||
                            mb_substr_count($parse->part_number_country->data_low, 'taiwan') ||
                            mb_substr_count($parse->part_number_country->data_low, 'korea') ||
                            mb_substr_count($parse->part_number_country->data_low, 'philippines') ||
                            mb_substr_count($parse->part_number_country->data_low, 'hong kong')
                        ){
                            $result = '
                              <b>
                                <span style="color:black;">Данное устройство произведено для Азии<br>
                                <span style="color:red;">Сейчас страны Азии не разблокируются.<br>
                                <span style="color:red; font-size:16px;">🚫 Отказ 🚫<br></span><br>
                              </b>
                            ';
                        }else{
                            $result = $clean;
                        }
                    }
                }


                ## Build information for out
                $result .= '<div style="font-size: 14px;">';
                foreach ($parse as $key => $value) {
                    if ($value->data) {
                        if($key == 'image_url') {
                            $result .= '<img src="' . $value->data . '" style="width: 180px; height: auto;"><br>';
                        } else {
                            if(
                                $key != 'meid' &&
                                $key != 'coverage_status' &&
                                $key != 'sold_by' &&
                                $key != 'part_type' &&
                                $key != 'registration_date'
                            ){
                                $result .= '<strong>' . $value->name_ru . '</strong>: ' . $value->data . '<br>';
                            }
                        }
                    }
                }
                $result .= '</div>';
            }
        }

        return '<div style="text-align: center;">' . $result . '</div>';
    }

    public function parse($html = ''): object
    {
        ## Set html data to parser
        $dom = HTMLDomParser::str_get_html($html);

        ## Get preview image
        if($html){
            if(isset($dom->find('pre', 0)->innertext)){
                $image = $dom->find('pre', 0)->find('img', 0);

                $data = (object) [
                    'image_url' => (object) [
                        'name' => 'Device image',
                        'data' => $image->src
                    ]
                ];
                $image->outertext = '';

                ## Get and convert all other data
                $pre = $dom->find('pre', 0)->innertext;

                $dom->clear();

                ## Set ru lang
                $ru_names = (object) array(
                    'description' => 'Описание устройства',
                    'model' => 'Модель',
                    'icloud_lock' => '☁ iCloud',
                    'icloud_status' => 'Статус iCloud',
                    'blacklist_status' => 'Черный список',
                    'estimated_purchase_date' => 'Дата покупки',
                    'warranty_start' => 'Начало гарантии',
                    'warranty_end' => 'Окончание гарантии',
                    'sim_lock' => 'Блокировка сим-карты',
                    'sold_by' => 'Продавец',
                    'part_number' => 'Номер модели',
                    'part_number_country' => 'Страна',
                    'model_number' => 'Модель'
                );

                foreach ($ru_names as $name => $row) {
                    $data->{$name} = (object) [
                        'name_ru' => $row,
                        'name' => null,
                        'data' => null,
                        'data_low' => null
                    ];
                }

                $pre_exp = explode('<br />', $pre);
                foreach ($pre_exp as $value) {
                    $line_exp = explode(':', $value);

                    if(count($line_exp) == 2){
                        $name = str_replace(array(' ', '-'), '_', mb_strtolower(trim($line_exp[0])));

                        $data->{$name} = (object) array(
                            'name_ru' => isset($ru_names->{$name}) ? $ru_names->{$name} : trim($line_exp[0]),
                            'name' => trim($line_exp[0]),
                            'data' => trim($line_exp[1]),
                            'data_low' => mb_strtolower(trim($line_exp[1]))
                        );
                    }
                }
            }else{
                $data = (object) [
                    'error' => 'No device image'
                ];
            }
        }else{
            $data = (object) [
                'error' => 'No html data'
            ];
        }

        return $data;
    }
}
