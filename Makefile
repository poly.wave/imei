# Makefile

build:
	echo "Set chmod to directories"
	echo "Building all containers:"
	docker-compose build

	sudo chmod -R 0777 src/vendor
	sudo chmod -R 0777 src/bootstrap
	sudo chmod -R 0777 src/storage
	sudo chmod 0777 src/composer.lock
	cd src && npm install

start:
	echo "Starting all containers:"
	docker-compose up -d

stop:
	echo "Stopping all containers:"
	docker-compose down -v

watch:
	echo "Watching build front:"
	cd src && npm run watch

dev:
	echo "Build front:"
	cd src && npm run dev

prod:
	echo "Build front:"
	cd src && npm run production

logs:
	docker-compose logs -f

console:
	docker exec -it app /bin/sh

list:
	docker ps

rebuilt:
	make stop
	make build
	make start
